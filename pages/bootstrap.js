// GRID container row col-md-2
// SPACES ps-md-2 mx-auto
// DISPLAY d-xl-flex justify-content-between

export default function bootstrap() {
	return (
		<div className="container">
			<div className={'pt-5 text-primary'}>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium animi culpa dolores ea et eum fugiat illo, libero odit perspiciatis quas qui temporibus veritatis vitae, voluptate. A est explicabo facilis nemo perspiciatis praesentium, ut veritatis voluptatem. Aliquam aspernatur, doloremque dolores inventore ipsam non provident quisquam quos saepe suscipit. Dicta, officia?
			</div>
		</div>
	)
}